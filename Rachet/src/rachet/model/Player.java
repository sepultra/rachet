package rachet.model;

import java.util.ArrayList;

/**
 * Thanks to the authors of PCL for this class. I was too lazy to encapsulate a
 * player class :-P
 * 
 * @author PCL
 */
public class Player {
	private ArrayList<Integer> items = new ArrayList<Integer>();
	private int id;
	private int color;
	private int head;
	private int face;
	private int neck;
	private int body;
	private int hand;
	private int feet;
	private int flag;
	private int photo;
	private int x;
	private int y;
	private int frame;
	private int total_membership_days;
	private int coins;
	private int egg_timer_remaining;
	private int age;
	private int banned_age;
	private int minutes_played;
	private int membership_days_remaining;
	private int timezone_offset;
	private int intRoomID;
	private int extRoomID;
	private long server_join_time;
	private String username;
	private String nickname;
	private String language_bitmask;
	private String uuid;
	private boolean is_member;
	private boolean is_safe;
	private boolean has_newspaper;

	public Player() {
		this.has_newspaper = false;
	}

	public Player(int id, String username, String uuid) {
		this.id = id;
		this.username = username;
		this.uuid = uuid;
	}

	public void parsePlayerObject(String packet) {
		if (packet.equals("")) {
			return;
		}

		String[] info = packet.split("\\|");

		this.id = Integer.parseInt(info[0]);
		this.nickname = info[1];
		this.language_bitmask = info[2];
		this.color = Integer.parseInt(info[3]);
		this.head = Integer.parseInt(info[4]);
		this.face = Integer.parseInt(info[5]);
		this.neck = Integer.parseInt(info[6]);
		this.body = Integer.parseInt(info[7]);
		this.hand = Integer.parseInt(info[8]);
		this.feet = Integer.parseInt(info[9]);
		this.flag = Integer.parseInt(info[10]);
		this.photo = Integer.parseInt(info[11]);
		this.x = Integer.parseInt(info[12]);
		this.y = Integer.parseInt(info[13]);
		this.frame = Integer.parseInt(info[14]);
		this.is_member = Boolean.parseBoolean(info[15]);
		this.total_membership_days = Integer.parseInt(info[16]);
	}

	public int getID() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public String getNickname() {
		return this.nickname;
	}

	public String getLanguage_Bitmask() {
		return this.language_bitmask;
	}

	public int getColor() {
		return this.color;
	}

	public int getHead() {
		return this.head;
	}

	public int getFace() {
		return this.face;
	}

	public int getNeck() {
		return this.neck;
	}

	public int getBody() {
		return this.body;
	}

	public int getHand() {
		return this.hand;
	}

	public int getFeet() {
		return this.feet;
	}

	public int getFlag() {
		return this.flag;
	}

	public int getPhoto() {
		return this.photo;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getFrame() {
		return this.frame;
	}

	public boolean getIsMember() {
		return this.is_member;
	}

	public int getTotal_Membership_Days() {
		return this.total_membership_days;
	}

	public int getIntRoomID() {
		return this.intRoomID;
	}

	public int getExtRoomID() {
		return this.extRoomID;
	}

	public String getUUID() {
		return this.uuid;
	}

	public int getCoins() {
		return this.coins;
	}

	public int getEggTimerRemaining() {
		return this.egg_timer_remaining;
	}

	public long getServerJoinTime() {
		return this.server_join_time;
	}

	public int getAge() {
		return this.age;
	}

	public int getBannedAge() {
		return this.banned_age;
	}

	public int getMinutesPlayed() {
		return this.minutes_played;
	}

	public int getMembershipDaysRemaining() {
		return this.membership_days_remaining;
	}

	public int getTimezoneOffset() {
		return this.timezone_offset;
	}

	public boolean getIsSafe() {
		return this.is_safe;
	}

	public boolean hasNewspaper() {
		return this.has_newspaper;
	}

	public boolean hasItem(int item_id) {
		if (this.items.contains(Integer.valueOf(item_id))) {
			return true;
		}
		return false;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void addItem(int item_id) {
		this.items.add(Integer.valueOf(item_id));
	}

	public void setColor(int color) {
		this.color = color;
	}

	public void setHead(int head) {
		this.head = head;
	}

	public void setFace(int face) {
		this.face = face;
	}

	public void setNeck(int neck) {
		this.neck = neck;
	}

	public void setBody(int body) {
		this.body = body;
	}

	public void setHand(int hand) {
		this.hand = hand;
	}

	public void setFeet(int feet) {
		this.feet = feet;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setRoom(int intRoomID, int extRoomID) {
		this.intRoomID = intRoomID;
		this.extRoomID = extRoomID;
	}

	public void setFrame(int frame) {
		this.frame = frame;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public void setEggTimerRemaining(int time) {
		this.egg_timer_remaining = time;
	}

	public void setServerJoinTime(long time) {
		this.server_join_time = time;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setBannedAge(int age) {
		this.banned_age = age;
	}

	public void setMinutesPlayed(int minutes) {
		this.minutes_played = minutes;
	}

	public void setMembershipDaysRemaining(int days) {
		this.membership_days_remaining = days;
	}

	public void setTimezoneOffset(int timezone) {
		this.timezone_offset = timezone;
	}

	public void setIsSafe(boolean bool) {
		this.is_safe = bool;
	}

	public void setNewspaper() {
		this.has_newspaper = (!this.has_newspaper);
	}

	public void setUUID(String uuid) {
		this.uuid = uuid;
	}
}