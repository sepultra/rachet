package rachet.core;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import rachet.net.codec.RachetFrameDecoder;
import rachet.net.RachetServerHandler;

/**
 * The main engine driving Rachet.
 * 
 * @author Lovelace
 */
public class Rachet {
	/**
	 * This is the host that Rachet will bind to. Please edit this if you want
	 * Rachet to bind to another IP. TODO Read constants from an INI file
	 */
	private static final String HOST = "localhost";
	/**
	 * This is the port that Rachet will bind to. This is the CPPS default from
	 * what I have seen. TODO Read constants from an INI file
	 */
	private static final int PORT = 6112;
	/**
	 * The logger we will use to log messages to sysout.
	 */
	private static final Logger logger = Logger.getLogger(Rachet.class.getName());

	public static void main(String[] args) throws Exception {
		ChannelFactory factory = new NioServerSocketChannelFactory(
				Executors.newCachedThreadPool(), Executors.newCachedThreadPool());

		ServerBootstrap bootstrap = new ServerBootstrap(factory);

		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			public ChannelPipeline getPipeline() {
				return Channels.pipeline(new RachetServerHandler(),
						new RachetFrameDecoder());
			}
		});

		bootstrap.setOption("child.tcpNoDelay", true);
		bootstrap.setOption("child.keepAlive", true);

		bootstrap.bind(new InetSocketAddress(HOST, PORT));

		logger.info("Rachet bound on " + HOST + ":" + PORT);
	}
}