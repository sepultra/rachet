package rachet.net.io;

import org.jboss.netty.buffer.ChannelBuffer;

/**
 * Decorates the {@link ChannelBuffer} class to provide some extra CPPS
 * functionality.
 * 
 * @author Lovelace
 */
public class Buffer {
	/**
	 * The underlying buffer.
	 */
	private ChannelBuffer buffer;

	/**
	 * Constructs a new {@link Buffer}.
	 * 
	 * @param buffer
	 *          The underlying buffer
	 */
	public Buffer(ChannelBuffer buffer) {
		this.buffer = buffer;
	}

	/**
	 * Send the argued packet over the channel.
	 * 
	 * @param packet
	 *          The packet to send
	 */
	public void sendPacket(String packet) {
		buffer.writeBytes((packet + "\000").getBytes());
	}

	/**
	 * Read a null-byte terminated string into the buffer and return it.
	 * 
	 * @return The read string
	 */
	public String readBytes() {
		byte b;
		StringBuilder builder = new StringBuilder();

		while ((b = buffer.readByte()) != 0)
			builder.append((char) b);

		return builder.toString();
	}
}