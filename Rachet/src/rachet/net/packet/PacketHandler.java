package rachet.net.packet;

import org.jboss.netty.buffer.ChannelBuffer;

/**
 * An interface representing a packet handler. The packet handler takes a
 * {@link Packet} and {@link ChannelBuffer} as arguments.
 * 
 * Note: Apparently the packets in CP are not opcode base, rather they are
 * string based.
 * 
 * @author Lovelace
 */
public interface PacketHandler {
	public void handle(Packet packet, ChannelBuffer buffer);
}