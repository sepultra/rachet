package rachet.net.packet;

import org.jboss.netty.channel.Channel;

/**
 * Represents an outgoing packet.
 * 
 * @author Lovelace
 */
public final class Packet {
	/**
	 * The short identifier for the packet, e.g "xt", "jr", etc.
	 */
	private final String id;
	/**
	 * The full packet array.
	 */
	private final String[] packet;
	/**
	 * The channel that this {@link Packet} is being sent over.
	 */
	private final Channel channel;

	/**
	 * Construct a new {@link Packet}.
	 * 
	 * @param id
	 *          The packet's ID
	 * @param packet
	 *          The full packet array (split by %)
	 * @param channel
	 *          The channel that the packet belongs to
	 */
	public Packet(String id, String[] packet, Channel channel) {
		this.id = id;
		this.packet = packet;
		this.channel = channel;
	}

	/**
	 * Returns the ID.
	 * 
	 * @return The ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the packet array.
	 * 
	 * @return The packet array
	 */
	public String[] getPacket() {
		return packet;
	}

	/**
	 * Returns the channel.
	 * 
	 * @return The channel
	 */
	public Channel getChannel() {
		return channel;
	}
}