package rachet.net.packet;

import java.util.HashMap;

import org.jboss.netty.buffer.ChannelBuffer;

import rachet.net.packet.impl.DefaultPacketHandler;

/**
 * Contains a map of all the existing packet handlers and handles packets.
 * 
 * @author Lovelace
 */
public class PacketManager {
	/**
	 * A map of packet handlers.
	 */
	private static final HashMap<String, PacketHandler> mass = new HashMap<String, PacketHandler>();

	/**
	 * Handles the argued packet.
	 * 
	 * @param packet
	 *          The {@link Packet} to handle
	 * @param buffer
	 *          The {@link ChannelBuffer}
	 */
	public static void handle(Packet packet, ChannelBuffer buffer) {
		if (mass.containsKey(packet.getId())) {
			mass.get(packet.getId()).handle(packet, buffer);
			return;
		}
		mass.get("default").handle(packet, buffer);
	}

	static {
		mass.put("default", new DefaultPacketHandler());
	}
}