package rachet.net.packet.impl;

import java.util.logging.Logger;

import org.jboss.netty.buffer.ChannelBuffer;

import rachet.net.packet.Packet;
import rachet.net.packet.PacketHandler;

/**
 * The default packet handler. This packet handler is called when there are no
 * handlers to invoke (the packet id is not recognized/implemented).
 * 
 * @author Lovelace
 */
public class DefaultPacketHandler implements PacketHandler {
	@Override
	public void handle(Packet packet, ChannelBuffer buffer) {
		Logger.getLogger(DefaultPacketHandler.class.getName()).warning(
				"Unhandled packet: " + packet.getId());
	}
}