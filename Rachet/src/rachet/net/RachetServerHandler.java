package rachet.net;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

/**
 * An instance of the {@link SimpleChannelHandler}. Notifies us when we are
 * connected.
 * 
 * @author Lovelace
 */
public class RachetServerHandler extends SimpleChannelHandler {
	@Override
	public void channelConnected(ChannelHandlerContext ctx,
			ChannelStateEvent event) throws Exception {
		System.out.println("Channel connected: " + ctx.getChannel().toString());
	}
}