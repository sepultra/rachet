package rachet.net.codec;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import rachet.net.io.Buffer;
import rachet.net.packet.Packet;
import rachet.net.packet.PacketManager;

/**
 * Repeatedly reads packets sent from the client and acts upon them.
 * 
 * @author Lovelace
 */
public class RachetFrameDecoder extends FrameDecoder {
	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel channel,
			ChannelBuffer buf) throws Exception {
		Buffer buffer = new Buffer(buf);
		String string = buffer.readBytes();
		if (!string.contains("%"))
			return null;
		String[] packet = string.split("%");

		Packet pac = new Packet(packet[1], packet, channel);
		PacketManager.handle(pac, buf);
		return pac;
	}
}